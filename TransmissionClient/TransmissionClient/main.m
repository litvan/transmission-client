//
//  main.m
//  TransmissionClient
//
//  Created by Ivan Litovchenko on 8/31/15.
//  Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
