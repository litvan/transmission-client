//
// Created by Ivan Litovchenko on 6/13/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import "CustomNavigationController.h"
#import "UIColor+TransmissionColors.h"


@implementation CustomNavigationController {

}


- (void)viewDidLoad {
    [super viewDidLoad];


//    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
//                                                         forBarMetrics:UIBarMetricsDefault];

    [[UINavigationBar appearance] setBarTintColor:[UIColor customGrey]];
    [[UINavigationBar appearance] setTranslucent:YES];
    [[UINavigationBar appearance] setTitleTextAttributes:
            @{
                    NSForegroundColorAttributeName : [UIColor customRed],
                    NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:21.0]}];
}


@end