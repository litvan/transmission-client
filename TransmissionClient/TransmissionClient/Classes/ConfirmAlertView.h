//
// Created by Ivan Litovchenko on 9/9/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface ConfirmAlertView : UIAlertView <UIAlertViewDelegate>
- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle completion:(void (^)(NSInteger buttonIndex))completion;
@end