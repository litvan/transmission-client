//
// Created by Ivan Litovchenko on 9/9/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface FilterFolderViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@property(nonatomic, strong) NSMutableSet *foundFolders;

@property(nonatomic, strong) NSMutableSet *selectedFolders;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)skipNavButtunAction:(id)sender;


@end