//
// Created by Ivan Litovchenko on 5/13/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import <TransmissionClientSDK/TCSTransmissionApi.h>
#import <TransmissionClientSDK/TCSBaseTorrent.h>
#import <TransmissionClientSDK/TCSAddedTorrent.h>
#import "TorrentsIndexTableViewController.h"
#import "TorrentTableCell.h"
#import "UIViewController+CacheRowHight.h"
#import "FilterFolderViewController.h"
#import "UIViewController+LoadingIndicator.h"
#import "Constants.h"
#import "UpdateTorrentManager.h"

@interface TorrentsIndexTableViewController ()
@property(atomic, strong) NSMutableArray *torrents;
@property(nonatomic, strong) TCSBaseTorrent *selectedTorrent;
@property(nonatomic, strong) TCSAddedTorrent *addedTorrent;
@property(nonatomic, strong) UIRefreshControl *refreshControl;
@property(nonatomic) bool viewAppeared;
@property(nonatomic, copy) void (^refreshCompletion)();
@end

@implementation TorrentsIndexTableViewController {

}

#pragma mark - UIViewController overridden methods

- (void)viewDidLoad {
    [super viewDidLoad];

    self.torrents = @[].mutableCopy;

    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor grayColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];


    UINib *cellNib = [UINib nibWithNibName:@"TorrentTableCellView" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"TorrentTableCell"];

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;

    self.tableView.tableFooterView = [[UIView alloc] init];

    __block __weak TorrentDetailViewController *torrentDetailViewController = self.torrentDetailViewController;

    self.refreshCompletion = ^{
        [torrentDetailViewController reloadAll];
    };

    self.viewAppeared = false;

}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self refresh:self.refreshCompletion];
    self.refreshCompletion = nil;

    if (self.viewAppeared) {
        [self refresh];
    }

    self.viewAppeared = true;


    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        self.torrentDetailViewController = nil;
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addTorrentFinishedNotification:) name:kAddTorrentFinishedNotification object:nil];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView reloadData];

}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [self unsubscribeFromTimer];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Refresh methods

- (void)refreshVisibleRowsWithCompletion:(void (^)())completion {

    self.torrents = [UpdateTorrentManager sharedInstance].torrents;

    //todo [self findFolders];

    dispatch_async(dispatch_get_main_queue(), ^{

        [self.tableView reloadData];

        if (completion) {
            completion();
        }


    });

}

- (void)refreshVisibleRows {

    [self refreshVisibleRowsWithCompletion:nil];

}


- (void)refresh:(void (^)())completion {

    [[UpdateTorrentManager sharedInstance] reloadTorrentsList:^(NSError *error) {

        dispatch_async(dispatch_get_main_queue(), ^() {
            self.noItemsLabel.hidden = [UpdateTorrentManager sharedInstance].torrents.count > 0;
        });

        if (!error) {
            self.torrents = [UpdateTorrentManager sharedInstance].torrents;

            dispatch_async(dispatch_get_main_queue(), ^{

                [self.refreshControl endRefreshing];
                [[self tableView] reloadData];

                if (completion) {
                    completion();
                }

                [self subscribeToTimer];
            });
        } else {
            [self unsubscribeFromTimer];

            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self tableViewReloadData:self.tableView];
            }];

            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];

            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:alertController animated:YES completion:nil];
            });

            self.torrents = nil;

        }
    }];

}


- (void)refresh {

    [self refresh:nil];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];

    if ([segue.identifier isEqualToString:@"detailSegue"]) {
        TorrentDetailViewController *torrentDetailViewController = (TorrentDetailViewController *) segue.destinationViewController;
        torrentDetailViewController.delegate = self;
        self.torrentDetailViewController = torrentDetailViewController;
    } else if ([segue.identifier isEqualToString:@"filterSegue"]) {
        FilterFolderViewController *filterFolderViewController = (FilterFolderViewController *) [(UINavigationController *) segue.destinationViewController viewControllers].firstObject;
        filterFolderViewController.foundFolders = [UpdateTorrentManager sharedInstance].foundFolders;
        filterFolderViewController.selectedFolders = [UpdateTorrentManager sharedInstance].selectedFolders;
    };

}

#pragma mark - UITableView implemented methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.torrents.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    TorrentTableCell *torrentTableCell = (TorrentTableCell *) [tableView dequeueReusableCellWithIdentifier:@"TorrentTableCell" forIndexPath:indexPath];
    TCSBaseTorrent *transmissionTorrent = self.torrents[(NSUInteger) indexPath.row];
    [torrentTableCell loadTorrent:transmissionTorrent];
    self.nameLabelWidth = torrentTableCell.nameLabel.frame.size.width;

    if (![self isEstimatedRowHeightInCache:indexPath]) {
        CGSize cellSize = [torrentTableCell systemLayoutSizeFittingSize:CGSizeMake(self.view.frame.size.width, 0) withHorizontalFittingPriority:1000.0 verticalFittingPriority:50.0];
        [self putEstimatedCellHeightToCache:indexPath height:cellSize.height];
    }

    [torrentTableCell.changeTorrentStatusButton addTarget:self action:@selector(changeTorrentStatusButtonAction:) forControlEvents:UIControlEventTouchDown];

    return torrentTableCell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self getEstimatedCellHeightFromCache:indexPath defaultHeight:100.f];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    [UpdateTorrentManager sharedInstance].selectedTorrent = self.torrents[(NSUInteger) indexPath.row];

    if (self.torrentDetailViewController) {
        [self.torrentDetailViewController reloadAll];
        [self.torrentDetailViewController hideTorrentIsNotSelected];
    } else {
        [self performSegueWithIdentifier:@"detailSegue" sender:self];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {

    [self unsubscribeFromTimer];

}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath {

    [self subscribeToTimer];
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (editingStyle == UITableViewCellEditingStyleDelete) {

        TCSBaseTorrent *baseTorrent = self.torrents[indexPath.row];

        [self unsubscribeFromTimer];

        __block BOOL deleteLocalFiles = false;

        dispatch_group_t group = dispatch_group_create();

        dispatch_group_enter(group);
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"Would you like to delete local files?" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            deleteLocalFiles = true;
            dispatch_group_leave(group);
        }]];

        [alertController addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            deleteLocalFiles = false;
            dispatch_group_leave(group);
        }]];

        [self presentViewController:alertController animated:YES completion:nil];

        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            [TCSTransmissionApi removeTorrent:baseTorrent deleteLocalFiles:deleteLocalFiles completion:^(NSError *error) {
                [self refresh];
                [self.torrentDetailViewController showToreentIsNotSelected];
            }];
        });

    }
}

#pragma mark - heap

- (void)subscribeToTimer {
    [self unsubscribeFromTimer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTimerNotification:) name:kUpdateTimerNotification object:nil];
}

- (void)unsubscribeFromTimer {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateTimerNotification object:nil];
}


- (void)changeTorrentStatusButtonAction:(UIButton *)button {

    TorrentTableCell *torrentTableCell = (TorrentTableCell *) button.superview.superview;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:torrentTableCell];
    TCSBaseTorrent *baseTorrent = self.torrents[indexPath.row];

    button.enabled = NO;

    [self unsubscribeFromTimer];

    [self showLoadingIndicator];


    __block dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_signal(semaphore);
    });

    [TCSTransmissionApi changeTorrentStatus:baseTorrent completion:^(TCSBaseTorrent *torrent, NSString *message, NSError *error) {

        [self refresh:^{
            [self subscribeToTimer];
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
            [self hideLoadingIndicator];
        }];

    }];

}

#pragma mark Notifications

- (void)updateTimerNotification:(NSNotification *)notification {
    [self refreshVisibleRows];
}

- (void)addTorrentFinishedNotification:(NSNotification *)notification {

    dispatch_async(dispatch_get_main_queue(), ^{

        [[UpdateTorrentManager sharedInstance].selectedFolders removeAllObjects];
        [self refreshVisibleRowsWithCompletion:^{
            TCSBaseTorrent *selectedTorrent = [UpdateTorrentManager sharedInstance].selectedTorrent;
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.torrents indexOfObject:selectedTorrent] inSection:0];

            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }];
    });

}

#pragma mark - TorrentDetailViewController delegate

- (void)refreshDetailData:(void (^)())completion {
    [self refreshVisibleRowsWithCompletion:completion];
}


@end