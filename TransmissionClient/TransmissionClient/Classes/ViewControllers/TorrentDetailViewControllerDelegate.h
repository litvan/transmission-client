//
// Created by Ivan Litovchenko on 6/13/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class TCSBaseTorrent;
@protocol TorrentDetailViewControllerDelegate;
@class TorrentIsNotSelectedView;


@interface TorrentDetailViewController : UIViewController <UISplitViewControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property(weak, nonatomic) IBOutlet UITableView *torrentTableView;
@property(weak, nonatomic) IBOutlet UITableView *torrentFeaturesTableView;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
@property(weak, nonatomic) IBOutlet UISegmentedControl *torrentDetailsSegment;
@property(nonatomic, strong) TCSFullTorrent *fullTorrent;
@property(nonatomic, weak) NSObject <TorrentDetailViewControllerDelegate> *delegate;

- (void)showToreentIsNotSelected;

- (void)hideTorrentIsNotSelected;

- (IBAction)torrentDetailsSegmentChangeAction:(id)sender;

- (void)reloadAll;

- (void)reload;
@end


@protocol TorrentDetailViewControllerDelegate

- (void)refreshDetailData:(void (^)())completion;

@end