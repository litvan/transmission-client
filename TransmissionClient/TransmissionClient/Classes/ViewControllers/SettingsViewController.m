//
// Created by Ivan Litovchenko on 9/13/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <TransmissionClientSDK/TCSSetting.h>
#import <TransmissionClientSDK/TCSTransmissionApi.h>
#import "SettingsViewController.h"
#import "TextSettingTableCell.h"


@interface SettingsViewController ()
@property(nonatomic, strong) JSONModelArray *settings;
@property(nonatomic, strong) NSIndexPath *reponderIndexPath;
@end

@implementation SettingsViewController {

}

#pragma mark - UIVieController overridden

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.tableFooterView = [[UIView alloc] init];
    self.settings = [TCSTransmissionApi allSettings];

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapGestureRecognizer];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

//    [TCSTransmissionApi saveSettings:self.settings];
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - UITableViewDataSource and UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    TCSSetting *setting = self.settings[indexPath.row];
    UITableViewCell *cell = nil;

    TextSettingTableCell *textSettingTableCell = (TextSettingTableCell *) [tableView dequeueReusableCellWithIdentifier:@"TextSettingCell"];
    textSettingTableCell.settingLabel.text = setting.name;
    textSettingTableCell.settingTextField.text = setting.value;
    [textSettingTableCell.settingTextField addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    textSettingTableCell.settingTextField.secureTextEntry = NO;
    [textSettingTableCell.settingTextField addTarget:self action:@selector(textFieldEditingDidBegin:) forControlEvents:UIControlEventEditingDidBegin];

    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];

    toolbar.items = @[
            [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
            [[UIBarButtonItem alloc] initWithTitle:@"Prev" style:UIBarButtonItemStyleDone target:self action:@selector(toolBarPrevButtonAction:)],
            [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(toolBarNextButtonAction:)],
    ];

    textSettingTableCell.settingTextField.inputAccessoryView = toolbar;

    switch (setting.type) {

        case SettingTextField: {


        }
            break;
        case SettingNumberField:

            textSettingTableCell.settingTextField.keyboardType = UIKeyboardTypeDecimalPad;

            break;
        case SettingPasswordField:

            textSettingTableCell.settingTextField.secureTextEntry = YES;

            break;
        case SettingSwitch:
            break;
        case SettingCheck:
            break;
    }

    cell = textSettingTableCell;

    cell = cell ?: [[UITableViewCell alloc] init];

    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

//    [self setResponderAtIndexPath:self.reponderIndexPath];

}


- (void)toolBarPrevButtonAction:(UIBarButtonItem *)barButton {

    NSIndexPath *prevIndexPath = [NSIndexPath indexPathForRow:self.reponderIndexPath.row - 1 inSection:self.reponderIndexPath.section];

    if (prevIndexPath.row < 0) {
        return;
    }

    self.reponderIndexPath = prevIndexPath;

    [self.tableView scrollToRowAtIndexPath:self.reponderIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    [self setResponderAtIndexPath:self.reponderIndexPath];

}


- (void)toolBarNextButtonAction:(UIBarButtonItem *)barButton {

    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForRow:self.reponderIndexPath.row + 1 inSection:self.reponderIndexPath.section];

    if (nextIndexPath.row >= [self.tableView numberOfRowsInSection:nextIndexPath.section]) {
        return;
    }

    self.reponderIndexPath = nextIndexPath;
    [self.tableView scrollToRowAtIndexPath:self.reponderIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];

    [self setResponderAtIndexPath:self.reponderIndexPath];

}

- (void)setResponderAtIndexPath:(NSIndexPath *)path {
    TextSettingTableCell *settingTableCell = (TextSettingTableCell *) [self.tableView cellForRowAtIndexPath:self.reponderIndexPath];
    [settingTableCell.settingTextField becomeFirstResponder];
}

- (void)textFieldEditingDidBegin:(UITextField *)textField {

    UITableViewCell *tableViewCell = (UITableViewCell *) textField.superview.superview;
    self.reponderIndexPath = [self.tableView indexPathForCell:tableViewCell];

}

- (void)textFieldChanged:(UITextField *)textField {

    TextSettingTableCell *cell = (TextSettingTableCell *) textField.superview.superview;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    TCSSetting *setting = self.settings[indexPath.row];

    setting.value = textField.text;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [TCSTransmissionApi allSettings].count;
}


#pragma mark - Notifications


- (void)keyboardWillHidden:(NSNotification *)notification {
    self.tableViewBottomspaceConstraint.constant = 0;
    [self.view layoutIfNeeded];
}

- (void)keyboardWillShow:(NSNotification *)notification {

    NSValue *keyboardRectObject = notification.userInfo[UIKeyboardFrameEndUserInfoKey];

    if (keyboardRectObject) {
        CGRect keyboardRect = keyboardRectObject.CGRectValue;
        self.tableViewBottomspaceConstraint.constant = keyboardRect.size.height + 10;
        [self.view layoutIfNeeded];
    }

}

#pragma mark - Gestures actions

- (void)tapGestureAction:(UITapGestureRecognizer *)tapgestureRecognizer {

    [self.view endEditing:YES];

}


@end