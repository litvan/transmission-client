//
// Created by Ivan Litovchenko on 6/13/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//




#import <TransmissionClientSDK/TCSTransmissionApi.h>
#import <TransmissionClientSDK/TCSBaseTorrent.h>
#import <TransmissionClientSDK/TCSFullTorrent.h>
#import <TransmissionClientSDK/TCSFile.h>
#import <TransmissionClientSDK/TCSFileStats.h>
#import <TransmissionClientSDK/TCSPeer.h>
#import "TorrentDetailViewControllerDelegate.h"
#import "UIColor+TransmissionColors.h"
#import "TorrentTableCell.h"
#import "FileTableCell.h"
#import "NSNumber+BytesConverter.h"
#import "PeerTableCell.h"
#import "UIViewController+CacheRowHight.h"
#import "UIViewController+LoadingIndicator.h"
#import "Constants.h"
#import "UpdateTorrentManager.h"
#import "TorrentIsNotSelectedView.h"


typedef NS_ENUM(NSInteger, SelectedSegmentType) {
    SelectedSegmentFiles,
    SelectedSegmentPeers
};

@interface TorrentDetailViewController ()
@property(nonatomic, strong) TorrentIsNotSelectedView *torrentIsNotSelectedView;
@end

@implementation TorrentDetailViewController {

}

#pragma mark UIViewController overridden methods

- (void)viewDidLoad {
    [super viewDidLoad];

    UINib *cellNib = [UINib nibWithNibName:@"TorrentTableCellView" bundle:[NSBundle mainBundle]];

    [self.torrentTableView registerNib:cellNib forCellReuseIdentifier:@"TorrentTableCell"];

    [self.torrentFeaturesTableView registerNib:cellNib forCellReuseIdentifier:@"TorrentTableCell"];

    self.torrentTableView.rowHeight = UITableViewAutomaticDimension;
    self.torrentTableView.estimatedRowHeight = 100;

    self.torrentFeaturesTableView.rowHeight = UITableViewAutomaticDimension;
    self.torrentFeaturesTableView.estimatedRowHeight = 60;

    [self torrentIsNotSelectedSetup];

}

- (void)torrentIsNotSelectedSetup {
    self.torrentIsNotSelectedView = [TorrentIsNotSelectedView loadFromNib];
    [self.view addSubview:self.torrentIsNotSelectedView];
    self.torrentIsNotSelectedView.frame = self.view.bounds;
    self.torrentIsNotSelectedView.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTimerNotification:) name:kUpdateTimerNotification object:nil];


}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];


    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        [self reloadAll];
    } else {
        [self reload];
    }
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

#pragma

- (void)showToreentIsNotSelected {

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view bringSubviewToFront:self.torrentIsNotSelectedView];
        self.torrentIsNotSelectedView.hidden = NO;
    });

}

- (void)hideTorrentIsNotSelected {

    dispatch_async(dispatch_get_main_queue(), ^{
        self.torrentIsNotSelectedView.hidden = YES;
    });
}

#pragma mark - Table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    NSInteger rowsCount = 0;
    TCSBaseTorrent *selectedTorrent = [UpdateTorrentManager sharedInstance].selectedTorrent;

    if (selectedTorrent && [tableView isEqual:self.torrentTableView]) {
        rowsCount = 1;
    }
    else if ([tableView isEqual:self.torrentFeaturesTableView]) {

        switch ((SelectedSegmentType) self.torrentDetailsSegment.selectedSegmentIndex) {
            case SelectedSegmentFiles:
                rowsCount = self.fullTorrent.files.count;
                break;
            case SelectedSegmentPeers:
                rowsCount = self.fullTorrent.peers.count;
                break;
        }


    }


    return rowsCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1f;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {

    CGFloat height = [self getEstimatedCellHeightFromCache:indexPath defaultHeight:100.f];

    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = nil;

    if ([tableView isEqual:self.torrentTableView]) {
        cell = [self fillInTorrentTableCellForTableview:tableView atIndexPath:indexPath];
    } else if ([tableView isEqual:self.torrentFeaturesTableView]) {

        switch ((SelectedSegmentType) self.torrentDetailsSegment.selectedSegmentIndex) {
            case SelectedSegmentFiles: {
                cell = [self fillInFileTableCellForTableView:tableView atIndexPath:indexPath];
            }
                break;
            case SelectedSegmentPeers:
                cell = [self fillInPeerTableCellForTableView:tableView atIndexPath:indexPath];
                break;
        };

    }

    cell = cell ?: [[UITableViewCell alloc] init];

    if (![self isEstimatedRowHeightInCache:indexPath]) {
        CGSize cellSize = [cell systemLayoutSizeFittingSize:CGSizeMake(self.view.frame.size.width, 0) withHorizontalFittingPriority:1000.0 verticalFittingPriority:50.0];
        [self putEstimatedCellHeightToCache:indexPath height:cellSize.height];
    }

    return cell;
}

#pragma mark - TableCells

- (UITableViewCell *)fillInTorrentTableCellForTableview:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {

    TorrentTableCell *torrentTableCell = (TorrentTableCell *) [tableView dequeueReusableCellWithIdentifier:@"TorrentTableCell" forIndexPath:indexPath];
    TCSBaseTorrent *transmissionTorrent = [UpdateTorrentManager sharedInstance].selectedTorrent;
    [torrentTableCell loadTorrent:transmissionTorrent];
    torrentTableCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [torrentTableCell.changeTorrentStatusButton addTarget:self action:@selector(changeTorrentStatusButtonAction:) forControlEvents:UIControlEventTouchDown];

    return torrentTableCell;
}

- (void)changeTorrentStatusButtonAction:(UIButton *)button {

    TCSBaseTorrent *baseTorrent = [UpdateTorrentManager sharedInstance].selectedTorrent;

    __weak __block TorrentDetailViewController *weakSelf = self;

    dispatch_async(dispatch_get_main_queue(), ^{
        [self showLoadingIndicator];
    });

    button.enabled = NO;

    __block dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_signal(semaphore);
    });

    [TCSTransmissionApi changeTorrentStatus:baseTorrent completion:^(TCSBaseTorrent *torrent, NSString *message, NSError *error) {

        [UpdateTorrentManager sharedInstance].selectedTorrent = torrent;

        dispatch_async(dispatch_get_main_queue(), ^{
            if (weakSelf.delegate != nil) {
                [weakSelf.delegate refreshDetailData:^{
                    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                    [self reload];
                    [self hideLoadingIndicator];
                }];
            }
            else {

                dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                [self reload];
                [self hideLoadingIndicator];
            }
        });

    }];
}

- (UITableViewCell *)fillInPeerTableCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {

    PeerTableCell *peerTableCell = (PeerTableCell *) [tableView dequeueReusableCellWithIdentifier:@"PeerCell"];
    TCSPeer *peer = self.fullTorrent.peers[indexPath.row];

    NSString *addressString = [NSString stringWithFormat:@"Address: %@ \n", peer.address];
    NSMutableAttributedString *address = [[NSMutableAttributedString alloc] initWithString:addressString];

    [address addAttributes:@{
            NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:12.f]
    }                range:NSMakeRange(0, 8)];
    [address addAttributes:@{
            NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Light" size:12.f],
    }                range:NSMakeRange(9, addressString.length - 9)];

    NSString *progressString = [NSString stringWithFormat:@"Progress: %.0f%%\n", peer.progress.floatValue * 100];
    NSMutableAttributedString *progress = [[NSMutableAttributedString alloc] initWithString:progressString];

    [progress addAttributes:@{
            NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:12.f]
    }                 range:NSMakeRange(0, 9)];
    [progress addAttributes:@{
            NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Light" size:12.f],
    }                 range:NSMakeRange(10, progressString.length - 10)];


    NSString *uploadString = [NSString stringWithFormat:@"Upload: %@/s", peer.rateToPeer.bytesConvertedValue];
    NSMutableAttributedString *upload = [[NSMutableAttributedString alloc] initWithString:uploadString];

    [upload addAttributes:@{
            NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:12.f]
    }               range:NSMakeRange(0, 7)];
    [upload addAttributes:@{
            NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Light" size:12.f],
    }               range:NSMakeRange(7, uploadString.length - 7)];


    [address appendAttributedString:progress];
    [address appendAttributedString:upload];

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 1;
    paragraphStyle.maximumLineHeight = 15;
    paragraphStyle.minimumLineHeight = 15;

    [address addAttributes:@{
            NSParagraphStyleAttributeName : paragraphStyle
    }                range:NSMakeRange(0, address.length)];

    peerTableCell.peerDataLabel.attributedText = address;


    peerTableCell.selectionStyle = UITableViewCellSelectionStyleNone;

    return peerTableCell;
}

- (UITableViewCell *)fillInFileTableCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {

    FileTableCell *fileTableCell = (FileTableCell *) [tableView dequeueReusableCellWithIdentifier:@"FileCell"];

    TCSFile *file = self.fullTorrent.files[indexPath.row];
    TCSFileStats *fileStats = self.fullTorrent.fileStats[indexPath.row];

    fileTableCell.fileNameLabel.text = file.name.lastPathComponent;
    BOOL fileIsCompleted = file.bytesCompleted.unsignedIntegerValue == file.length.unsignedIntegerValue;

    fileTableCell.fileSwitch.userInteractionEnabled = !fileIsCompleted;

    if (!fileTableCell.fileSwitch.userInteractionEnabled) {
        fileTableCell.fileSwitch.onTintColor = [UIColor customGrey];
    } else {
        fileTableCell.fileSwitch.onTintColor = [UIColor switchGreen];
    }

    fileTableCell.fileSwitch.on = (BOOL) fileStats.wanted;
    [fileTableCell.fileSwitch addTarget:self action:@selector(torrentFileSwitchChangedAction:) forControlEvents:UIControlEventValueChanged];
    fileTableCell.selectionStyle = UITableViewCellSelectionStyleNone;

    float percentsDone = file.bytesCompleted.unsignedIntegerValue / file.length.unsignedIntegerValue * 100;
    fileTableCell.fileExchangeDetailsLabel.text = [NSString stringWithFormat:@"%.1f %% uploaded of %@", percentsDone, file.length.bytesConvertedValue];

    return fileTableCell;


}

#pragma mark - Actions

- (void)torrentFileSwitchChangedAction:(UISwitch *)uiSwitch {

    FileTableCell *fileTableCell = (FileTableCell *) uiSwitch.superview.superview;
    NSIndexPath *indexPath = [self.torrentFeaturesTableView indexPathForCell:fileTableCell];

    if (uiSwitch.on) {
        [TCSTransmissionApi startUploadCertainFileInTorrent:self.fullTorrent atIndex:indexPath.row completion:^(NSError *error) {
            [self reloadAll];
        }];
    } else {
        [TCSTransmissionApi stopUploadCertainFileInTorrent:self.fullTorrent atIndex:indexPath.row completion:^(NSError *error) {
            [self reloadAll];
        }];
    }

}

- (IBAction)torrentDetailsSegmentChangeAction:(id)sender {
    [self tableViewReloadData:self.torrentFeaturesTableView];
}

#pragma mark - Refresh methods

- (void)reloadAll {

    [self reload];

    [self retrieveFullTorrent];
}


- (void)reload {

    dispatch_async(dispatch_get_main_queue(), ^{
        [self tableViewReloadData:self.torrentTableView];

        if (self.fullTorrent == nil) {
            [self retrieveFullTorrent];
        }

        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        self.tableViewHeightConstraint.constant = [self.torrentTableView rectForRowAtIndexPath:indexPath].size.height;
        [self.view layoutIfNeeded];

    });

}

#pragma mark - Notifications


- (void)updateTimerNotification:(NSNotification *)notification {
    [self reload];
}


#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController
     willHideViewController:(UIViewController *)viewController
          withBarButtonItem:(UIBarButtonItem *)barButtonItem
       forPopoverController:(UIPopoverController *)popoverController {

    barButtonItem.title = NSLocalizedString(@"Torrents", @"Torrents");
    [self.navigationItem setLeftBarButtonItem:barButtonItem
                                     animated:YES];
}

- (void)splitViewController:(UISplitViewController *)splitController
     willShowViewController:(UIViewController *)viewController
  invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem {
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
}

#pragma mark - helpMethods

- (void)retrieveFullTorrent {

    TCSBaseTorrent *baseTorrent = [UpdateTorrentManager sharedInstance].selectedTorrent;

    [TCSTransmissionApi getFullTorrentWithBaseTorrent:baseTorrent completion:^(TCSFullTorrent *torrent, NSString *message, NSError *error) {

        self.fullTorrent = torrent;

        dispatch_async(dispatch_get_main_queue(), ^{
            [self tableViewReloadData:self.torrentFeaturesTableView];
        });

    }];
}

@end