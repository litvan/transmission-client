//
// Created by Ivan Litovchenko on 5/13/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
#import "TorrentDetailViewControllerDelegate.h"

@class TorrentDetailViewController;

@interface TorrentsIndexTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, TorrentDetailViewControllerDelegate>
@property(nonatomic, strong) TorrentDetailViewController *torrentDetailViewController;
@property(nonatomic) CGFloat nameLabelWidth;
@property(weak, nonatomic) IBOutlet UITableView *tableView;
@property(weak, nonatomic) IBOutlet UILabel *noItemsLabel;

@end