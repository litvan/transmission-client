//
// Created by Ivan Litovchenko on 9/9/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import "FilterFolderViewController.h"
#import "UpdateTorrentManager.h"


@interface FilterFolderViewController ()
@property(nonatomic, strong) NSArray *foundFoldersArray;
@end

@implementation FilterFolderViewController {

}


- (void)viewDidLoad {
    [super viewDidLoad];

    UIBarButtonItem *selectAllBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Select all"
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self
                                                                          action:@selector(selectAllBarButtonAction:)];

    UIBarButtonItem *selectNoneBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Select none" style:UIBarButtonItemStylePlain target:self action:@selector(selectNoneBarButtonAction:)];

    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpace.width = 20.f;

    self.navigationItem.rightBarButtonItems = @[selectAllBarButton, fixedSpace, selectNoneBarButton];


    self.foundFoldersArray = self.foundFolders.allObjects;
    self.tableView.tableFooterView = [[UIView alloc] init];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [[UpdateTorrentManager sharedInstance] forceUpdate];
}


- (void)selectNoneBarButtonAction:(UIBarButtonItem *)selectNoneAction {

    [self.selectedFolders removeAllObjects];
    [self.tableView reloadData];
}

- (void)selectAllBarButtonAction:(UIBarButtonItem *)selectAllAction {

    [self.selectedFolders addObjectsFromArray:self.foundFoldersArray];
    [self.tableView reloadData];

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.foundFoldersArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    cell = cell ?: [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
    NSString *folderName = self.foundFoldersArray[indexPath.row];

    cell.textLabel.text = folderName;

    if ([self.selectedFolders containsObject:folderName]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *folderName = self.foundFoldersArray[indexPath.row];

    if ([self.selectedFolders containsObject:folderName]) {
        [self.selectedFolders removeObject:folderName];
    } else {
        [self.selectedFolders addObject:folderName];
    }

    [self.tableView reloadData];
}

- (IBAction)skipNavButtunAction:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end