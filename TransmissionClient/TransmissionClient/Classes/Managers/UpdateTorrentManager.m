//
// Created by Ivan Litovchenko on 10/9/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <TransmissionClientSDK/TCSTransmissionApi.h>
#import <TransmissionClientSDK/TCSBaseTorrent.h>
#import <UIKit/UIKit.h>
#import <TransmissionClientSDK/TCSAddedTorrent.h>
#import "UpdateTorrentManager.h"
#import "Constants.h"


@interface UpdateTorrentManager ()
@property(nonatomic, strong) NSTimer *scheduledTimer;
@property(nonatomic) BOOL active;
@end

@implementation UpdateTorrentManager {

}

#pragma mark - Getterrs and Setters

- (TCSBaseTorrent *)selectedTorrent {

    if (_selectedTorrent == nil && _torrents.count > 0) {
        _selectedTorrent = _torrents.firstObject;
    }

    return _selectedTorrent;
}

#pragma mark Singleton Methods


+ (instancetype)sharedInstance {
    static UpdateTorrentManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        self.scheduledTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(torrentWasAddedNotification:) name:kAddFileTCSNotification object:nil];

    }
    return self;
}

- (void)setupTimer {
    [self.scheduledTimer invalidate];
    self.scheduledTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
    self.active = YES;
}

- (void)stopTimer {
    self.active = NO;
    [self.scheduledTimer invalidate];
}

- (void)timerAction:(NSTimer *)timerAction {

    [self forceUpdate];
}


-(void)forceUpdate{

    [self reloadTorrentsList:^(NSError *error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateTimerNotification object:nil];
    }];

}

- (void)reloadTorrentsList:(void (^)(NSError *error))completion {

    [TCSTransmissionApi indexTorrentFilesWithCompletion:^(NSMutableArray *items, NSString *message, NSError *responseError) {

        if (!responseError) {
            self.torrents = items;
            [self refreshSelectedTorrent];

            if (!self.active) {
                [self setupTimer];
            }

            if (completion) {
                [self findFolders];
                completion(responseError);
            }

        }

    }];
}


- (void)refreshSelectedTorrent {

    if (!self.selectedTorrent) {
        return;
    }

    for (TCSBaseTorrent *baseTorrent in self.torrents) {

        if ([baseTorrent.id isEqualToNumber:self.selectedTorrent.id]) {

            self.selectedTorrent = baseTorrent;

            break;
        }

    }

}

- (void)torrentWasAddedNotification:(NSNotification *)notification {

    NSData *data = notification.userInfo[@"addedTorrentData"];
//    UIWindow *window = [[UIApplication sharedApplication] keyWindow];

    UIAlertController *startDownloadAlertController = [UIAlertController alertControllerWithTitle:nil message:@"Would you like to start downlod now?" preferredStyle:UIAlertControllerStyleAlert];

    __block BOOL paused = true;
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_t newFolderGroup = dispatch_group_create();
    dispatch_group_t selectFolderGroup = dispatch_group_create();

    [startDownloadAlertController addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        paused = false;
        dispatch_group_leave(group);
    }]];

    [startDownloadAlertController addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        dispatch_group_leave(group);
    }]];

    dispatch_group_enter(selectFolderGroup);
    dispatch_group_enter(group);
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:startDownloadAlertController animated:YES completion:nil];

    __block NSMutableArray *foundFolders = self.foundFolders.allObjects.mutableCopy;

    [foundFolders insertObject:@"Add new folder" atIndex:0];

    if (![foundFolders containsObject:kTCSRootFolderCaption]){
        [foundFolders insertObject:kTCSRootFolderCaption atIndex:1];
    }

    __block NSString *selectedFolder = nil;

    __block UIAlertController *newFolderAlertController = [UIAlertController alertControllerWithTitle:@"Add new folder" message:@"Input folder name" preferredStyle:UIAlertControllerStyleAlert];

    [newFolderAlertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Folder name";
    }];

    [newFolderAlertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        selectedFolder = [newFolderAlertController.textFields.firstObject.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        dispatch_group_leave(newFolderGroup);
        dispatch_group_leave(selectFolderGroup);
    }]];

    UIAlertController *folderChooseAlertViewController = [UIAlertController alertControllerWithTitle:@"Choose folder" message:@"Chose folder where to download" preferredStyle:UIAlertControllerStyleAlert];

//    folderChooseAlertViewController.popoverPresentationController.sourceView = window;
//    folderChooseAlertViewController.popoverPresentationController.sourceRect = window.frame;

    for (NSString *foundFolder in foundFolders) {

        UIAlertAction *action = [UIAlertAction actionWithTitle:foundFolder style:UIAlertActionStyleDefault handler:^(UIAlertAction *alertAction) {
            NSUInteger selectedIndex = [folderChooseAlertViewController.actions indexOfObject:alertAction];

            if (selectedIndex == 0) {
                dispatch_group_enter(newFolderGroup);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:newFolderAlertController animated:YES completion:nil];
                });
            } else if (selectedIndex == 1) {
                selectedFolder = nil;
                dispatch_group_leave(selectFolderGroup);
            }
            else {
                selectedFolder = foundFolders[selectedIndex];
                dispatch_group_leave(selectFolderGroup);
            }

        }];

        [folderChooseAlertViewController addAction:action];
    }


    void (^addTorrentBlock)() = ^{
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

        __block TCSAddedTorrent *addedTorrent = nil;
        __block BOOL duplicate = false;

        [TCSTransmissionApi addTorrentFileWithCompletion:^(TCSAddedTorrent *responseAddedTorrent, BOOL responseDuplicate, NSError *error) {

            if (error == nil) {
                addedTorrent = responseAddedTorrent;
            }
            duplicate = responseDuplicate;
            dispatch_semaphore_signal(semaphore);

        }                                         folder:selectedFolder torrentData:data paused:paused];

        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);

        if (addedTorrent == nil) {
            return;
        }

        [self reloadTorrentsList:^(NSError *error) {
            [self findAddedTorrent:addedTorrent];
            [[NSNotificationCenter defaultCenter] postNotificationName:kAddTorrentFinishedNotification object:nil];
        }];

        if (duplicate) {
            [self showDuplicateMessage];
        }

    };

    dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        if (foundFolders.count != 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:folderChooseAlertViewController animated:YES completion:nil];
            });
        } else {
            dispatch_group_leave(selectFolderGroup);
        }


    });


    dispatch_group_notify(selectFolderGroup, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        addTorrentBlock();
    });


}

- (void)showDuplicateMessage {

    dispatch_async(dispatch_get_main_queue(), ^{

        UIAlertController *duplicateAler = [UIAlertController alertControllerWithTitle:nil message:@"File was allready added" preferredStyle:UIAlertControllerStyleAlert];
        [duplicateAler addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];

        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:duplicateAler animated:YES completion:nil];

    });

}

- (void)findAddedTorrent:(TCSAddedTorrent *)addedTorrent {

    for (NSUInteger i = 0; i < self.torrents.count; i++) {

        TCSBaseTorrent *baseTorrent = self.torrents[i];

        if (baseTorrent.id == addedTorrent.id) {
            self.selectedTorrent = baseTorrent;
            break;
        }

    }

}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}


- (void)findFolders {

    NSString *needlePath = [TCSTransmissionApi downloadDir];
    NSMutableSet *foundItems = [[NSMutableSet alloc] init];
    NSArray *torrents = (id) self.torrents.copy;

    for (NSInteger index = 0; index < torrents.count; index++) {
        TCSBaseTorrent *baseTorrent = torrents[index];

        NSRange range = [baseTorrent.downloadDir rangeOfString:needlePath];
        NSRange folderRange = NSMakeRange(range.length, baseTorrent.downloadDir.length - range.length);
        NSString *foundSubstring = [baseTorrent.downloadDir substringWithRange:folderRange];
        foundSubstring = [foundSubstring stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"/"]];

        if (foundSubstring.length == 0) {
            foundSubstring = kTCSRootFolderCaption;
        }

        if (self.selectedFolders.count && ![self.selectedFolders containsObject:foundSubstring]) {
            [self.torrents removeObject:baseTorrent];
        }

        if (foundSubstring.length > 0) {
            [foundItems addObject:foundSubstring];
        }
    }

    self.foundFolders = foundItems;
    self.selectedFolders = self.selectedFolders ?: [[NSMutableSet alloc] initWithSet:self.foundFolders];

}


@end