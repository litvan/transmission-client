//
// Created by Ivan Litovchenko on 10/9/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TCSBaseTorrent;

@interface UpdateTorrentManager : NSObject


@property(nonatomic, strong) NSMutableArray *torrents;

@property(nonatomic) TCSBaseTorrent *selectedTorrent;

@property(nonatomic, strong) NSMutableSet *selectedFolders;

@property(nonatomic, strong) NSMutableSet *foundFolders;

+ (instancetype)sharedInstance;

- (void)setupTimer;

- (void)stopTimer;

- (void)forceUpdate;

- (void)reloadTorrentsList:(void (^)(NSError *))completion;
@end