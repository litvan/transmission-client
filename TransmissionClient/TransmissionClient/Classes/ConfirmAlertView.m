//
// Created by Ivan Litovchenko on 9/9/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import "ConfirmAlertView.h"


@interface ConfirmAlertView ()
@property(nonatomic, copy) void (^completion)(NSInteger buttonIndex);
@end

@implementation ConfirmAlertView {

}


- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
             otherButtonTitle:(NSString *)otherButtonTitle
                   completion:(void (^)(NSInteger buttonIndex))completion {

    self = [super initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:cancelButtonTitle, otherButtonTitle, nil];
    if (self) {

        self.delegate = self;
        self.completion = completion;

    }

    return self;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (self.completion) {
        self.completion(buttonIndex);
    }

}


@end