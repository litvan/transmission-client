//
// Created by Ivan Litovchenko on 6/13/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CustomNavigationController : UINavigationController
@end