//
// Created by Ivan Litovchenko on 5/23/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import "AutoLabel.h"


@implementation AutoLabel {

}

- (void)setBounds:(CGRect)bounds {
    if (bounds.size.width != self.bounds.size.width) {
        [self setNeedsUpdateConstraints];
    }
    [super setBounds:bounds];
}

- (void)updateConstraints {
    if (self.preferredMaxLayoutWidth != self.bounds.size.width) {
        self.preferredMaxLayoutWidth = self.bounds.size.width;
    }
    [super updateConstraints];
}


@end