//
// Created by Ivan Litovchenko on 9/10/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface FileTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fileNameLabel;
@property (weak, nonatomic) IBOutlet UISwitch *fileSwitch;
@property (weak, nonatomic) IBOutlet UILabel *fileExchangeDetailsLabel;
@end