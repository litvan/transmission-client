//
// Created by Ivan Litovchenko on 9/18/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface TextSettingTableCell : UITableViewCell{}
@property (weak, nonatomic) IBOutlet UILabel *settingLabel;
@property (weak, nonatomic) IBOutlet UITextField *settingTextField;

@end