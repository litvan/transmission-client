//
// Created by Ivan Litovchenko on 9/12/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface PeerTableCell : UITableViewCell {}

@property (weak, nonatomic) IBOutlet UILabel *peerDataLabel;
@end