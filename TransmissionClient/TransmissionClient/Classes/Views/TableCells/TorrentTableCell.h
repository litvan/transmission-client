//
// Created by Ivan Litovchenko on 5/13/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

@interface TorrentTableCell : UITableViewCell{}

@property (weak, nonatomic) IBOutlet UIProgressView *proggressView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *exchangeDataLabel;
@property (weak, nonatomic) IBOutlet UIButton *changeTorrentStatusButton;
@property (weak, nonatomic) IBOutlet UILabel *exchangeStatusLabel;


@property(nonatomic, strong) NSString *exchangeFormat;

- (void)loadTorrent:(TCSBaseTorrent *)transmissionTorrent;
@end