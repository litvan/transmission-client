//
// Created by Ivan Litovchenko on 5/13/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import <TransmissionClientSDK/TCSBaseTorrent.h>
#import <TransmissionClientSDK/TCSConstants.h>
#import "TorrentTableCell.h"
#import "NSNumber+BytesConverter.h"


@implementation TorrentTableCell {

}


- (void)loadTorrent:(TCSBaseTorrent *)transmissionTorrent {

    self.proggressView.progress = transmissionTorrent.percentDone.floatValue;
    self.nameLabel.text = transmissionTorrent.name;
    self.exchangeDataLabel.text = [NSString stringWithFormat:@"%@, uploaded %@", transmissionTorrent.sizeWhenDone.bytesConvertedValue, transmissionTorrent.uploadedEver.bytesConvertedValue];
    self.changeTorrentStatusButton.enabled = YES;


    UIImage *image = nil;

    switch (transmissionTorrent.status) {
        case StatusSeeding:
            image = [UIImage imageNamed:@"pauseIcon"];
            break;
        case StatusDownloading:
            image = [UIImage imageNamed:@"pauseIcon"];
            break;
        case StatusQueued:
            image = [UIImage imageNamed:@"pauseIcon"];
            break;
        case StatusPaused:
            image = [UIImage imageNamed:@"playIcon"];
            break;
        default:
            image = [UIImage imageNamed:@"playIcon"];
            break;
    }

    NSMutableString *exchangeText = [[NSMutableString alloc] init];

    if (transmissionTorrent.rateUpload.intValue > 0) {
        [exchangeText appendFormat:@"Upload: %@/s\n", [transmissionTorrent.rateUpload bytesConvertedValue]];
    }
    if (transmissionTorrent.rateDownload.intValue > 0) {
        [exchangeText appendFormat:@"Download: %@/s\n", [transmissionTorrent.rateDownload bytesConvertedValue]];
    }

    if (exchangeText.length > 0) {
        exchangeText = [exchangeText substringToIndex:exchangeText.length - 1].mutableCopy;
    }

    if (!exchangeText.length) {
        [exchangeText appendString:@"-"];
    }

    self.exchangeStatusLabel.text = exchangeText;

    [self.changeTorrentStatusButton setBackgroundImage:image forState:UIControlStateNormal];
    [self.changeTorrentStatusButton setHighlighted:NO];
    self.backgroundColor = [UIColor clearColor];

//    [self setNeedsUpdateConstraints];
//    [self updateConstraintsIfNeeded];
    [self layoutIfNeeded];


}
@end