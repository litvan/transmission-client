//
// Created by Ivan Litovchenko on 2/4/16.
// Copyright (c) 2016 litvan.com. All rights reserved.
//

#import "BaseView.h"


@implementation BaseView {

}

+(instancetype)loadFromNib{

    return  [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:0].lastObject;
}

@end