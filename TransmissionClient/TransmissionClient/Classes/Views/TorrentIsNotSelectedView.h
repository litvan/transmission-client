//
// Created by Ivan Litovchenko on 2/4/16.
// Copyright (c) 2016 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseView.h"


@interface TorrentIsNotSelectedView : BaseView
@end