//
// Created by Ivan Litovchenko on 5/12/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import <Foundation/Foundation.h>


#import <TransmissionClientSDK/TCSConstants.h>

extern NSString *const kUpdateTimerNotification;
extern NSString *const kAddTorrentFinishedNotification;


@interface Constants : NSObject
@end