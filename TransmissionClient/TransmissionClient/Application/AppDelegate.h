//
//  AppDelegate.h
//  Transmission-client
//
//  Created by Ivan Litovchenko on 5/8/15.
//  Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

