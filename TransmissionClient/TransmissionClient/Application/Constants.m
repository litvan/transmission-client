//
// Created by Ivan Litovchenko on 5/12/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import "Constants.h"

NSString *const kUpdateTimerNotification = @"kUpdateTimerNotification";
NSString *const kAddTorrentFinishedNotification = @"kAddTorrentFinishedNotification";


@implementation Constants {

}
@end