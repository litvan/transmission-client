//
//  AppDelegate.m
//  Transmission-client
//
//  Created by Ivan Litovchenko on 5/8/15.
//  Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import <TransmissionClientSDK/TCSTransmissionApi.h>
#import <TransmissionClientSDK/TCSPreferences.h>
#import <Crashlytics/Crashlytics.h>
#import <Fabric/Fabric.h>
#import "AppDelegate.h"
#import "TorrentDetailViewControllerDelegate.h"
#import "TorrentsIndexTableViewController.h"
#import "UIColor+TransmissionColors.h"
#import "UpdateTorrentManager.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {


    [TCSPreferences setDefaults];

    [Fabric with:@[[Crashlytics class]]];

    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];

    self.window.tintColor = [UIColor customRed];

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {

        UISplitViewController *splitViewController = (UISplitViewController *) [storyboard instantiateViewControllerWithIdentifier:@"iPadStartController"];

        TorrentDetailViewController *torrentDetailViewController = [splitViewController.viewControllers.lastObject viewControllers].lastObject;
        splitViewController.delegate = torrentDetailViewController;
        [torrentDetailViewController showToreentIsNotSelected];

        TorrentsIndexTableViewController *torrentsIndexTableViewController = [splitViewController.viewControllers.firstObject viewControllers].lastObject;
        torrentsIndexTableViewController.torrentDetailViewController = torrentDetailViewController;

        self.window.rootViewController = splitViewController;
    }
    else {

        TorrentsIndexTableViewController *torrentsIndexTableViewController = (TorrentsIndexTableViewController *) [storyboard instantiateViewControllerWithIdentifier:@"PhoneStartController"];
        self.window.rootViewController = torrentsIndexTableViewController;

    }

    [UpdateTorrentManager sharedInstance];

    [self.window makeKeyAndVisible];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [[UpdateTorrentManager sharedInstance] stopTimer];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[UpdateTorrentManager sharedInstance] setupTimer];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {

    NSString *type;
    [url getResourceValue:&type forKey:NSURLTypeIdentifierKey error:nil];

    [TCSTransmissionApi addTorrentFromUrl:url];

    return NO;
}


@end
