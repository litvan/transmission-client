//
// Created by Ivan Litovchenko on 9/20/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <objc/runtime.h>
#import "UIViewController+LoadingIndicator.h"

static const void *backgroundKey = @"backgroundKey";

@implementation UIViewController (LoadingIndicator)

- (UIView *)background {
    return objc_getAssociatedObject(self, backgroundKey);
}

- (void)setBackground:(UIView *)view {

    objc_setAssociatedObject(self, backgroundKey, view, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

}


- (void)showLoadingIndicator {

    UIWindow *window = [UIApplication sharedApplication].keyWindow;

    self.background = [[UIView alloc] initWithFrame:window.bounds];
    self.background.backgroundColor = [UIColor colorWithRed:0.f green:0.f blue:0.f alpha:0.3f];

    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.background addSubview:activityIndicatorView];

    activityIndicatorView.center = self.background.center;
    [activityIndicatorView startAnimating];

    [window addSubview:self.background];
    [window bringSubviewToFront:self.background];

}

- (void)hideLoadingIndicator {
    [self.background removeFromSuperview];
}

@end