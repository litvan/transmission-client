//
// Created by Ivan Litovchenko on 9/20/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIViewController (LoadingIndicator)
- (void)showLoadingIndicator;

- (void)hideLoadingIndicator;
@end