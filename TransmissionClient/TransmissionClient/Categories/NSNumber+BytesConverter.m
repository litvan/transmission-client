//
// Created by Ivan Litovchenko on 5/13/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import "NSNumber+BytesConverter.h"


@implementation NSNumber (BytesConverter)


- (NSString *)bytesConvertedValue {


    if (self.unsignedIntegerValue > 1024 && self.unsignedIntegerValue < powf(1024, 2)) {
        return [NSString stringWithFormat:@"%.1f KiB", self.floatValue / 1024];
    }

    if (self.unsignedIntegerValue > powf(1024, 2) && self.unsignedIntegerValue < powf(1024, 3)) {
        return [NSString stringWithFormat:@"%.1f MiB", self.floatValue / powf(1024, 2)];
    }

    if (self.unsignedIntegerValue > powf(1024, 3) && self.unsignedIntegerValue < powf(1024, 4)) {
        return [NSString stringWithFormat:@"%.1f GiB", self.floatValue / powf(1024, 3)];
    }

    if (self.unsignedIntegerValue > powf(1024, 4) && self.unsignedIntegerValue < powf(1024, 5)) {
        return [NSString stringWithFormat:@"%.1f TiB", self.floatValue / powf(1024, 4)];
    }

    if (self.unsignedIntegerValue > powf(1024, 5) && self.unsignedIntegerValue < powf(1024, 6)) {
        return [NSString stringWithFormat:@"%.1f PiB", self.floatValue / powf(1024, 5)];
    }

    if (self.unsignedIntegerValue > powf(1024, 6) && self.unsignedIntegerValue < powf(1024, 7)) {
        return [NSString stringWithFormat:@"%.1f EiB", self.floatValue / powf(1024, 6)];
    }

    if (self.unsignedIntegerValue > powf(1024, 7) && self.unsignedIntegerValue < powf(1024, 8)) {
        return [NSString stringWithFormat:@"%.1f ZiB", self.floatValue / powf(1024, 7)];
    }

    if (self.unsignedIntegerValue > powf(1024, 8)) {
        return [NSString stringWithFormat:@"%.1f YiB", self.floatValue / powf(1024, 8)];
    }


    return [NSString stringWithFormat:@"%lu B", (unsigned long)[self unsignedIntegerValue]];
}

@end