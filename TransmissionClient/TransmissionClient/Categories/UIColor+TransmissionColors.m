//
// Created by Ivan Litovchenko on 6/13/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import "UIColor+TransmissionColors.h"


@implementation UIColor (TransmissionColors)

+ (UIColor *)colorFromHexString:(NSString *)hexString alpha:(CGFloat)alpha {
    uint rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16) / 255.0f green:((rgbValue & 0xFF00) >> 8) / 255.0f blue:(rgbValue & 0xFF) / 255.0f alpha:alpha];
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {

    return [self colorFromHexString:hexString alpha:1];

}

+ (UIColor *)customLightBlue {
    return [UIColor colorWithRed:0.f / 255.0f green:245.f / 255.0f blue:241.f / 255.0f alpha:1.f];
}

+ (UIColor *)customRed {
    return [UIColor colorFromHexString:@"#d82f2f"];
}

+ (UIColor *)customGrey {
    return [UIColor colorFromHexString:@"#d9dada"];
}

+ (UIColor *)switchGreen {
    return [UIColor colorFromHexString:@"#4bd763"];
}
@end


