//
// Created by Ivan Litovchenko on 9/12/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import "UIViewController+CacheRowHight.h"

const void *estimatedRowHeightCacheKey = @"estimatedRowHeightCacheKey";

@implementation UIViewController (CacheRowHight)


- (NSMutableDictionary *)estimatedRowHeightCache {
    return objc_getAssociatedObject(self, estimatedRowHeightCacheKey);
}


- (void)setEstimatedRowHeightCache:(NSMutableDictionary *)object {
    return objc_setAssociatedObject(self, estimatedRowHeightCacheKey, object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}


#pragma mark - estimated height cache methods

// put height to cache
- (void)putEstimatedCellHeightToCache:(NSIndexPath *)indexPath height:(CGFloat)height {
    [self initEstimatedRowHeightCacheIfNeeded];
    [self.estimatedRowHeightCache setValue:[[NSNumber alloc] initWithFloat:height] forKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
}


// get height from cache
- (CGFloat)getEstimatedCellHeightFromCache:(NSIndexPath *)indexPath defaultHeight:(CGFloat)defaultHeight {
    [self initEstimatedRowHeightCacheIfNeeded];
    NSNumber *estimatedHeight = [self.estimatedRowHeightCache valueForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
    if (estimatedHeight != nil) {
        return [estimatedHeight floatValue];
    }
    return defaultHeight;
}

// check if height is on cache
- (BOOL)isEstimatedRowHeightInCache:(NSIndexPath *)indexPath {
    return [self getEstimatedCellHeightFromCache:indexPath defaultHeight:0] > 0;
}

// init cache
- (void)initEstimatedRowHeightCacheIfNeeded {
    if (self.estimatedRowHeightCache == nil) {
        [self setEstimatedRowHeightCache:(id) [[NSMutableDictionary alloc] init]];
    }
}

// custom [self.tableView reloadData]
- (void)tableViewReloadData:(UITableView *)tableView {

    dispatch_async(dispatch_get_main_queue(), ^{
        // clear cache on reload
        self.estimatedRowHeightCache = [[NSMutableDictionary alloc] init];
        [tableView reloadData];

        for (UITableViewCell *visibleCell in tableView.visibleCells) {
            [visibleCell layoutIfNeeded];
        }

    });

}


@end