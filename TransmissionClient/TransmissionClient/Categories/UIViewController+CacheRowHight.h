//
// Created by Ivan Litovchenko on 9/12/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIViewController (CacheRowHight) <UITableViewDelegate, UITableViewDataSource>
- (void)putEstimatedCellHeightToCache:(NSIndexPath *)indexPath height:(CGFloat)height;

- (CGFloat)getEstimatedCellHeightFromCache:(NSIndexPath *)indexPath defaultHeight:(CGFloat)defaultHeight;

- (BOOL)isEstimatedRowHeightInCache:(NSIndexPath *)indexPath;

- (void)initEstimatedRowHeightCacheIfNeeded;

- (id)estimatedRowHeightCache;

- (void)setEstimatedRowHeightCache:(NSMutableDictionary *)object;

- (void)tableViewReloadData:(UITableView *)tableView;
@end