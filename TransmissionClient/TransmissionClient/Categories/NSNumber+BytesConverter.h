//
// Created by Ivan Litovchenko on 5/13/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (BytesConverter)
- (NSString *)bytesConvertedValue;
@end