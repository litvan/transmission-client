//
// Created by Ivan Litovchenko on 5/9/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import "TCSRequestGenerator.h"
#import "objc/runtime.h"
#import "TCSPreferences.h"

NSString *const sessionIdKey = @"X-Transmission-Session-Id";
static NSString *sessionId = nil;
static NSDictionary *sessionDict = nil;


@implementation TCSRequestGenerator {

}

+ (NSString *)sessionId {
    return sessionId;
}

+ (void)setSessionId:(NSString *)value {
    sessionId = value;
}

+ (void)generateRequest:(id <NSURLSessionDelegate>)delegate request:(NSMutableURLRequest *)request completion:(CommonResponse)completion {

    NSURLSessionConfiguration *defaultConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:defaultConfiguration];

    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {

        NSHTTPURLResponse *httpUrlResponse = (NSHTTPURLResponse *) response;

        if ([[httpUrlResponse.allHeaderFields allKeys] containsObject:sessionIdKey]) {
            [self setSessionId:httpUrlResponse.allHeaderFields[sessionIdKey]];
        }

        if (!RequestIsSuccessful(httpUrlResponse.statusCode)) {
            sessionDict = nil;
        }

        if ([self sessionId] && httpUrlResponse.statusCode == 409) {

            [request setValue:[self sessionId] forHTTPHeaderField:sessionIdKey];
            NSURLSessionDataTask *repeatDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *repeatData, NSURLResponse *repeatResponse, NSError *repeatError) {

                if (repeatError == nil && sessionDict == nil) {
                    [self getSession:nil];
                }

                if (completion) completion(RequestIsSuccessful([(NSHTTPURLResponse *) repeatResponse statusCode]), repeatData, nil, repeatError);
            }];

            [repeatDataTask resume];
        } else {
            if (completion) completion(RequestIsSuccessful(httpUrlResponse.statusCode), data, nil, error);
        }

    }];

    [dataTask resume];


}

+ (void)getSession:(void (^)())completion {

    NSMutableURLRequest *request = [self setHeadersForRequest];
    request.HTTPMethod = @"POST";

    request.HTTPBody = [NSJSONSerialization
            dataWithJSONObject:@{
                    @"method" : @"session-get"
            } options:0 error:nil];

    [self generateRequest:nil request:request completion:^(BOOL success, NSData *data, NSString *message, NSError *error) {

        if (success) {
            sessionDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSString *downloadDir = sessionDict[@"arguments"][@"download-dir"];
            [TCSPreferences setDownloadDirectory:downloadDir];
        }

        if (completion) {
            completion();
        }
    }];

}

+ (NSMutableURLRequest *)setHeadersForRequest {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[TCSPreferences rpcUrl]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:60.0];


    NSString *credentials = [NSString stringWithFormat:@"%@:%@", [TCSPreferences userName], [TCSPreferences password]];
    NSData *credentialsData = [credentials dataUsingEncoding:NSUTF8StringEncoding];

    [request addValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:[NSString stringWithFormat:@"Basic %@", [credentialsData base64EncodedStringWithOptions:0]] forHTTPHeaderField:@"Authorization"];

    if ([self sessionId]) {
        [request addValue:[self sessionId] forHTTPHeaderField:@"X-Transmission-Session-Id"];
    }

    return request;
}

+ (void)generatePOSTRequestForJsonWithDictionary:(NSDictionary *)mapData completion:(CommonResponse)completion {

    NSMutableURLRequest *request = [self setHeadersForRequest];
    [self setHeadersForRequest];
    request.HTTPMethod = @"POST";

    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:nil];
    [request setHTTPBody:postData];

    [self generateRequest:nil request:request completion:^(BOOL success, NSData *responseData, NSString *message, NSError *error) {

        if (completion) completion(success, responseData, message, error);

    }];

}


@end