//
// Created by Ivan Litovchenko on 10/29/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TCSPreferences : NSObject

+ (BOOL)https;

+ (void)setHttps:(BOOL)https;

+ (NSString *)remoteHost;

+ (void)setRemoteHost:(NSString *)remoteHost;

+ (NSInteger)port;

+ (void)setPort:(NSInteger)port;

+ (NSString *)rpcPath;

+ (NSString *)userName;

+ (void)setUserName:(NSString *)userName;

+ (NSString *)password;

+ (void)setPassword:(NSString *)password;

+ (NSString *)downloadDirectory;

+ (void)setDownloadDirectory:(NSString *)downloadDirectory;

+ (void)setDefaults;

+ (NSURL *)rpcUrl;
@end