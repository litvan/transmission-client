//
// Created by Ivan Litovchenko on 10/29/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import "TCSPreferences.h"

NSString *const httpPreferenceKey = @"https_preference";
NSString *const remoteHostPreferenceKey = @"remote_host_preference";
NSString *const portPreferenceKey = @"port_preference";
NSString *const rpcPathPreferenceKey = @"rpc_path_preference";
NSString *const userNamePreferenceKey = @"user_name_preference";
NSString *const passwordPreferenceKey = @"password_preference";
NSString *const downloadDirectoryPreferenceKey = @"download_directory_preference";

@implementation TCSPreferences {

}

+ (BOOL)https {
    return [[NSUserDefaults standardUserDefaults] boolForKey:httpPreferenceKey];
}

+ (void)setHttps:(BOOL)https {

    [[NSUserDefaults standardUserDefaults] setBool:https forKey:httpPreferenceKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)remoteHost {
    return [[NSUserDefaults standardUserDefaults] objectForKey:remoteHostPreferenceKey];
}


+ (void)setRemoteHost:(NSString *)remoteHost {
    [[NSUserDefaults standardUserDefaults] setObject:remoteHost forKey:remoteHostPreferenceKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSInteger)port {
    return [[NSUserDefaults standardUserDefaults] integerForKey:portPreferenceKey];
}

+ (void)setPort:(NSInteger)port {
    [[NSUserDefaults standardUserDefaults] setInteger:port forKey:portPreferenceKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)rpcPath {

    return [[NSUserDefaults standardUserDefaults] objectForKey:rpcPathPreferenceKey];
}

+ (void)setRPCPath:(NSString *)rpcPath {

    [[NSUserDefaults standardUserDefaults] setObject:rpcPath forKey:rpcPathPreferenceKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)userName {

    return [[NSUserDefaults standardUserDefaults] objectForKey:userNamePreferenceKey];
}

+ (void)setUserName:(NSString *)userName {
    [[NSUserDefaults standardUserDefaults] setObject:userName forKey:userNamePreferenceKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)password {

    return [[NSUserDefaults standardUserDefaults] objectForKey:passwordPreferenceKey];
}

+ (void)setPassword:(NSString *)password {

    [[NSUserDefaults standardUserDefaults] setObject:password forKey:passwordPreferenceKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 * @deprecated
 */
+ (NSString *)downloadDirectory {
    return [[NSUserDefaults standardUserDefaults] objectForKey:downloadDirectoryPreferenceKey];
}

+ (void)setDownloadDirectory:(NSString *)downloadDirectory {
    [[NSUserDefaults standardUserDefaults] setObject:downloadDirectory forKey:downloadDirectoryPreferenceKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setDefaults {

    if ([self remoteHost].length == 0) {
        [self setRemoteHost:@"transmission.onlini.co"]; 
    }

    if ([self port] == 0) {
        [self setPort:9091];
    }

    if ([self rpcPath].length == 0) {
        [self setRPCPath:@"/transmission/rpc"];
    }

    if ([self userName].length == 0) {
        [self setUserName:@"transmission"];
    }

    if ([self password].length == 0) {
        [self setPassword:@"transmission"];
    }

    if ([self downloadDirectory].length == 0) {
        [self setDownloadDirectory:@"downloadDirectory"];
    }
}

+ (NSURL *)rpcUrl {

    NSURLComponents *components = [[NSURLComponents alloc] init];
    components.scheme = [TCSPreferences https] ? @"https" : @"http";
    components.host = [TCSPreferences remoteHost];
    components.port = @([TCSPreferences port]);
    components.path = [TCSPreferences rpcPath];

    return components.URL;
}

@end