//
// Created by Ivan Litovchenko on 5/9/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TCSRequestGenerator : NSObject
+ (void)generateRequest:(id <NSURLSessionDelegate>)delegate request:(NSMutableURLRequest *)request completion:(CommonResponse)completion;

+ (void)getSession:(void (^)())completion;

+ (void)generatePOSTRequestForJsonWithDictionary:(NSDictionary *)mapData completion:(CommonResponse)completion;
@end