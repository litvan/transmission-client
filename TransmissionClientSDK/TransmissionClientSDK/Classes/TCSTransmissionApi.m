//
// Created by Ivan Litovchenko on 5/12/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import "TCSTransmissionApi.h"
#import "TCSRequestGenerator.h"
#import "UIKit/UIKit.h"
#import "TCSBaseTorrent.h"
#import "TCSFullTorrent.h"
#import "TCSAddedTorrent.h"
#import "TCSSetting.h"

static NSString *settingsKey = @"transmissionSettings";

@implementation TCSTransmissionApi {

}

+ (JSONModelArray *)allSettings {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    return [[JSONModelArray alloc] initWithArray:[userDefaults objectForKey:settingsKey] modelClass:[TCSSetting class]];
}

+ (TCSSetting *)getSetting:(SettingKey)settingKey {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    NSArray *settings = [userDefaults objectForKey:settingsKey];

    if (settings && settings.count > settingKey) {
        return [[TCSSetting alloc] initWithDictionary:settings[settingKey] error:nil];
    }

    return nil;
}


+ (NSArray *)indexFields {
    return @[
            @"id",
            @"name",
            @"totalsize",
            @"downloadDir",
            @"percentDone",
            @"downloadedEver",
            @"sizeWhenDone",
            @"uploadedEver",
            @"status",
            @"rateDownload",
            @"rateUpload"
    ];
}


+ (NSArray *)allFields {
    return @[

            @"activityDate",
            @"addedDate",
            @"announceResponse",
            @"announceURL",
            @"bandwidthPriority",
            @"comment",
            @"corruptEver",
            @"creator",
            @"dateCreated",
            @"desiredAvailable",
            @"doneDate",
            @"downloadDir",
            @"downloadedEver",
            @"downloaders",
            @"downloadLimit",
            @"downloadLimited",
            @"error",
            @"errorString",
            @"eta",
            @"files",
            @"fileStats",
            @"hashString",
            @"haveUnchecked",
            @"haveValid",
            @"honorsSessionLimits",
            @"id",
            @"isPrivate",
            @"lastAnnounceTime",
            @"lastScrapeTime",
            @"leechers",
            @"leftUntilDone",
            @"manualAnnounceTime",
            @"maxConnectedPeers",
            @"name",
            @"nextAnnounceTime",
            @"nextScrapeTime",
            @"peer",
            @"limit",
            @"peers",
            @"peersConnected",
            @"peersFrom",
            @"peersGettingFromUs",
            @"peersKnown",
            @"peersSendingToUs",
            @"percentDone",
            @"pieces",
            @"pieceCount",
            @"pieceSize",
            @"priorities",
            @"rateDownload",
            @"rateUpload",
            @"recheckProgress",
            @"scrapeResponse",
            @"scrapeURL",
            @"seeders",
            @"seedRatioLimit",
            @"seedRatioMode",
            @"sizeWhenDone",
            @"startDate",
            @"status",
            @"swarmSpeed",
            @"timesCompleted",
            @"trackers",
            @"totalSize",
            @"torrentFile",
            @"uploadedEver",
            @"uploadLimit",
            @"uploadLimited",
            @"uploadRatio",
            @"wanted",
            @"webseeds",
            @"webseedsSendingToUs"

    ];
}

+ (NSDictionary *)composeProcedureRequestWithArguments:(NSDictionary *)arguments method:(NSString *)method {

    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] init];

    if (arguments != nil) {
        requestDictionary[@"arguments"] = arguments;
    }
    requestDictionary[@"method"] = method;

    return requestDictionary;
}


+ (NSDictionary *)composeTorrentGetProcedureRequest {

    return [self composeTorrentGetProcedureRequestWithIds:nil fields:[self indexFields]];
}

+ (NSDictionary *)composeTorrentGetProcedureRequestWithIds:(NSArray *)ids fields:(NSArray *)fields {

    fields = fields ?: [self indexFields];

    NSMutableDictionary *arguments = [@{@"fields" : fields} mutableCopy];

    if (ids) {
        arguments[@"ids"] = ids;
    }

    return [self composeProcedureRequestWithArguments:arguments
                                               method:@"torrent-get"];
}

+ (void)indexTorrentFilesWithCompletion:(IndexResponse)completion {

    [self indexTorrentFilesWithCompletion:completion sendingData:[self composeTorrentGetProcedureRequest]];

}


+ (void)indexTorrentFilesWithCompletion:(IndexResponse)completion sendingData:(NSDictionary *)sendingData {

    [TCSRequestGenerator generatePOSTRequestForJsonWithDictionary:sendingData completion:^(BOOL success, NSData *data, NSString *message, NSError *error) {

        if (completion && success) {

            NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

            if ([@"success" isEqualToString:jsonObject[@"result"]]) {

                NSMutableArray *jsonModelArray = [[NSMutableArray alloc] init];
                for (NSDictionary *torrentDictionary in jsonObject[@"arguments"][@"torrents"]) {
                    TCSBaseTorrent *baseTorrent = [[TCSBaseTorrent alloc] initWithDictionary:torrentDictionary error:nil];

                    if (![baseTorrent.downloadDir hasSuffix:@"pn"]) {
                        [jsonModelArray addObject:baseTorrent];
                    }
                }

                completion(jsonModelArray, message, error);
            }

        } else {
            completion(nil, nil, error);
        }

    }];

}

+ (void)refreshObjects:(NSMutableArray *)objects
            indexPaths:(NSArray *)indexPaths
            completion:(UpdateCompletion)completion {


    NSMutableArray *refreshIds = [[NSMutableArray alloc] init];

    for (NSIndexPath *indexPath in indexPaths) {

        TCSBaseTorrent *transmissionTorrent = objects[(NSUInteger) indexPath.row];
        [refreshIds addObject:transmissionTorrent.id];

    }

    [self indexTorrentFilesWithCompletion:^(NSMutableArray *array, NSString *string, NSError *error) {

        for (NSInteger i = 0; i < array.count; i++) {
            NSInteger rowIndex = [(NSIndexPath *) indexPaths[i] row];
            TCSBaseTorrent *transmissionTorrent = objects[rowIndex];
            TCSBaseTorrent *updatedTransmissionTorrent = array[i];

            if ([transmissionTorrent.id isEqualToNumber:updatedTransmissionTorrent.id]) {
                objects[rowIndex] = updatedTransmissionTorrent;
            }

        }

        if (completion)completion(YES);

//        NSLog(@"update %@", array);

    }                         sendingData:[self composeTorrentGetProcedureRequestWithIds:refreshIds fields:nil]];


}


+ (NSDictionary *)composeTorrentAddProcedureRequestWithData:(NSData *)data {

    return [self composeProcedureRequestWithArguments:
            @{
                    @"download-dir" : self.downloadDir,
                    @"metainfo" : [data base64EncodedStringWithOptions:0]
            }                                  method:@"torrent-add"];

}

+ (NSString *)downloadDir {
//    return [TCSPreferences downloadDirectory];
    NSString *downloadDir = [self getDownloadDir];

    return downloadDir;
}

+ (NSString *)getDownloadDir {


    NSDictionary *sendingData = [self composeProcedureRequestWithArguments:nil
                                                                    method:@"session-get"];

    dispatch_semaphore_t semafore = dispatch_semaphore_create(0);

    __block NSString *downloadDir = nil;

    [TCSRequestGenerator generatePOSTRequestForJsonWithDictionary:sendingData completion:^(BOOL success, NSData *data, NSString *message, NSError *error) {

        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        downloadDir = jsonObject[@"arguments"][@"download-dir"];
        dispatch_semaphore_signal(semafore);
    }];

    dispatch_semaphore_wait(semafore, DISPATCH_TIME_FOREVER);

    return downloadDir;
}


+ (void)addTorrentFileWithCompletion:(AddTorrentResponse)completion folder:(NSString *)folder torrentData:(NSData *)torrentData paused:(BOOL)paused {

    NSMutableDictionary *arguments = [[NSMutableDictionary alloc] init];

    dispatch_semaphore_t semafore = dispatch_semaphore_create(0);

    [TCSRequestGenerator getSession:^{
        dispatch_semaphore_signal(semafore);
    }];

    dispatch_semaphore_wait(semafore, DISPATCH_TIME_FOREVER);


    NSString *downloadDir = [self downloadDir];

    if (folder != nil && ![folder isEqualToString:kTCSRootFolderCaption] && folder.length > 0 && ![folder isEqualToString:@"root"]) {
        downloadDir = [downloadDir stringByAppendingPathComponent:folder];
    }

    [arguments addEntriesFromDictionary:@{
            @"download-dir" : downloadDir,
            @"metainfo" : [torrentData base64EncodedStringWithOptions:0],
            @"paused" : @((int) paused)
    }];


    NSDictionary *sendingData = [self composeProcedureRequestWithArguments:arguments
                                                                    method:@"torrent-add"];

    [TCSRequestGenerator generatePOSTRequestForJsonWithDictionary:sendingData completion:^(BOOL success, NSData *data, NSString *message, NSError *error) {

        if (completion && success) {

            NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

            if ([@"success" isEqualToString:jsonObject[@"result"]]) {

                NSDictionary *jsonArguments = jsonObject[@"arguments"];

                TCSAddedTorrent *addedTorrent = nil;
                BOOL duplicate = NO;

                if ([jsonArguments.allKeys containsObject:@"torrent-duplicate"]) {
                    addedTorrent = [[TCSAddedTorrent alloc] initWithDictionary:jsonArguments[@"torrent-duplicate"] error:nil];
                    duplicate = YES;
                } else {
                    addedTorrent = [[TCSAddedTorrent alloc] initWithDictionary:jsonArguments[@"torrent-added"] error:nil];
                    duplicate = NO;
                }

                completion(addedTorrent, duplicate, nil);


            }

        }


    }];


}

+ (void)addTorrentFromUrl:(NSURL *)url {

    if (![url.pathExtension isEqualToString:@"torrent"]) {
        return;
    }

    NSData *data = [[NSData alloc] initWithContentsOfURL:url];

    [[NSNotificationCenter defaultCenter] postNotificationName:kAddFileTCSNotification object:nil userInfo:@{@"addedTorrentData" : data}];

}

+ (void)getTorrentWithBaseTorrent:(TCSBaseTorrent *)torrent fields:(NSArray *)fields completion:(CommonResponse)completion {

    if (!torrent || !completion) {
        return;
    }

    NSDictionary *sendingData = [self composeTorrentGetProcedureRequestWithIds:@[torrent.id] fields:fields];

    [TCSRequestGenerator generatePOSTRequestForJsonWithDictionary:sendingData completion:^(BOOL success, NSData *data, NSString *message, NSError *error) {
        completion(success, data, message, error);
    }];

}

+ (void)getBaseTorrentWithBaseTorrent:(TCSBaseTorrent *)torrent completion:(BaseTorrentResponse)completion {

    if (completion == nil) {
        return;
    }

    [self getTorrentWithBaseTorrent:torrent fields:[self indexFields] completion:^(BOOL success, NSData *data, NSString *message, NSError *error) {
        if (success) {

            NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

            if ([@"success" isEqualToString:jsonObject[@"result"]]) {

                TCSBaseTorrent *fullTorrent = [[TCSBaseTorrent alloc] initWithDictionary:[jsonObject[@"arguments"][@"torrents"] lastObject] error:nil];
                completion(fullTorrent, message, error);
            }

        }
    }];

}

+ (void)getFullTorrentWithBaseTorrent:(TCSBaseTorrent *)torrent completion:(FullTorrentResponse)completion {

    if (completion == nil) {
        return;
    }

    [self getTorrentWithBaseTorrent:torrent fields:[self allFields] completion:^(BOOL success, NSData *data, NSString *message, NSError *error) {
        if (success) {

            NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

            if ([@"success" isEqualToString:jsonObject[@"result"]]) {

                TCSFullTorrent *fullTorrent = [[TCSFullTorrent alloc] initWithDictionary:[jsonObject[@"arguments"][@"torrents"] lastObject] error:nil];
                completion(fullTorrent, message, error);
            }

        }
    }];

}

+ (void)removeTorrent:(TCSBaseTorrent *)torrent deleteLocalFiles:(BOOL)deleteLocalFiles completion:(Response)completion {


    NSDictionary *sendingData = [self composeProcedureRequestWithArguments:
                    @{
                            @"ids" : @[torrent.id],
                            @"delete-local-data" : @((int) deleteLocalFiles)
                    }
                                                                    method:@"torrent-remove"];

    [TCSRequestGenerator generatePOSTRequestForJsonWithDictionary:sendingData completion:^(BOOL success, NSData *data, NSString *message, NSError *error) {

        if (completion && success) {

            NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

            if ([@"success" isEqualToString:jsonObject[@"result"]]) {
                completion(nil);
            }

        }

    }];


}

+ (void)changeTorrentStatus:(TCSBaseTorrent *)torrent completion:(BaseTorrentResponse)completion {

    NSString *method = nil;

    switch (torrent.status) {

        case StatusSeeding:
            method = @"torrent-stop";
            break;
        case StatusDownloading:
            method = @"torrent-stop";
            break;
        case StatusQueued:
            method = @"torrent-stop";
            break;
        case StatusPaused:
            method = @"torrent-start";
            break;

        default:
            method = @"torrent-start";
            break;
    }


    NSDictionary *sendingData = [self composeProcedureRequestWithArguments:
                    @{
                            @"ids" : @[torrent.id],
                    }
                                                                    method:method];

    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    [TCSRequestGenerator generatePOSTRequestForJsonWithDictionary:sendingData completion:^(BOOL success, NSData *data, NSString *message, NSError *error) {
        dispatch_semaphore_signal(semaphore);
    }];

    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    __block TCSBaseTorrent *receivedTorrent = torrent;

//    while (receivedTorrent.status == torrent.status) {
//
//        semaphore = dispatch_semaphore_create(0);
//
//        [self getBaseTorrentWithBaseTorrent:receivedTorrent completion:^(TCSBaseTorrent *requesteTorrent, NSString *message, NSError *error) {
//            receivedTorrent = requesteTorrent;
//            dispatch_semaphore_signal(semaphore);
//        }];
//
//        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
//
//    }

    if (completion) {
        completion(receivedTorrent, nil, nil);
    }


}

+ (void)startUploadCertainFileInTorrent:(TCSFullTorrent *)torrent atIndex:(NSInteger)index completion:(Response)completion {

    NSDictionary *sendingData = [self composeProcedureRequestWithArguments:
                    @{
                            @"ids" : @[torrent.id],
                            @"files-wanted" : @[@(index)],
                            @"priority-normal" : @[@(index)]
                    }
                                                                    method:@"torrent-set"];

    [TCSRequestGenerator generatePOSTRequestForJsonWithDictionary:sendingData completion:^(BOOL success, NSData *data, NSString *message, NSError *error) {

        if (completion && success) {

            NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

            if ([@"success" isEqualToString:jsonObject[@"result"]]) {
                completion(nil);
            }

        }

    }];

}

+ (void)stopUploadCertainFileInTorrent:(TCSFullTorrent *)torrent atIndex:(NSInteger)index completion:(Response)completion {

    NSDictionary *sendingData = [self composeProcedureRequestWithArguments:
                    @{
                            @"ids" : @[torrent.id],
                            @"files-unwanted" : @[@(index)],
                    }
                                                                    method:@"torrent-set"];

    [TCSRequestGenerator generatePOSTRequestForJsonWithDictionary:sendingData completion:^(BOOL success, NSData *data, NSString *message, NSError *error) {

        if (completion && success) {

            NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

            if ([@"success" isEqualToString:jsonObject[@"result"]]) {
                completion(nil);
            }

        }

    }];
}


@end