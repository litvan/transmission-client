//
// Created by Ivan Litovchenko on 5/12/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSConstants.h"

@class TCSBaseTorrent;
@class JSONModelArray;


@interface TCSTransmissionApi : NSObject

+ (JSONModelArray *)allSettings;

+ (void)indexTorrentFilesWithCompletion:(void (^)(NSMutableArray *, NSString *, NSError *))completion;

+ (void)refreshObjects:(NSMutableArray *)objects indexPaths:(NSArray *)indexPaths completion:(UpdateCompletion)completion;

+ (void)addTorrentFromUrl:(NSURL *)url;

+ (void)getFullTorrentWithBaseTorrent:(TCSBaseTorrent *)torrent completion:(FullTorrentResponse)completion;

+ (void)removeTorrent:(TCSBaseTorrent *)torrent deleteLocalFiles:(BOOL)deleteLocalFiles completion:(Response)completion;

+ (void)changeTorrentStatus:(TCSBaseTorrent *)torrent completion:(BaseTorrentResponse)completion;

+ (void)startUploadCertainFileInTorrent:(TCSFullTorrent *)torrent atIndex:(NSInteger)index completion:(Response)completion;

+ (void)stopUploadCertainFileInTorrent:(TCSFullTorrent *)torrent atIndex:(NSInteger)index completion:(Response)completion;

+ (id)downloadDir;

+ (void)addTorrentFileWithCompletion:(AddTorrentResponse)completion folder:(NSString *)folder torrentData:(NSData *)torrentData paused:(BOOL)paused;

@end