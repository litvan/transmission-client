//
// Created by Ivan Litovchenko on 8/28/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import "TCSConstants.h"
#import "TCSAddedTorrent.h"
#import "TCSBaseTorrent.h"


NSInteger const kHTTP_Ok = 200;
NSInteger const kHTTP_Conflict = 409;

NSString *tcsErrorDomain = @"com.litvan.transmission.sdk";
NSString *const kTCSRootFolderCaption = @"Root folder";

NSString *const kAddFileTCSNotification = @"kAddFileNotification";

@implementation TCSConstants {

}
@end