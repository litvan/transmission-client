//
// Created by Ivan Litovchenko on 8/28/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TCSFullTorrent;
@class TCSAddedTorrent;
@class TCSBaseTorrent;

extern NSString *tcsErrorDomain;
extern NSString *const kTCSRootFolderCaption;

#define RequestIsSuccessful(code) (code>=200 && code<300)

typedef void (^CommonResponse)(BOOL success, NSData *data, NSString *message, NSError *error);

typedef void (^IndexResponse)(NSMutableArray *items, NSString *message, NSError *error);

typedef void (^BaseTorrentResponse)(TCSBaseTorrent *, NSString *message, NSError *error);
typedef void (^FullTorrentResponse)(TCSFullTorrent *, NSString *message, NSError *error);

typedef void (^UpdateCompletion)(BOOL success);

typedef void (^AddTorrentResponse)(TCSAddedTorrent *addedTorrent,BOOL duplicate,  NSError *error);

typedef void (^Response)(NSError *error);


extern NSInteger const kHTTP_Ok;
extern NSInteger const kHTTP_Conflict;
extern NSString *const kAddFileTCSNotification;

typedef NS_ENUM(NSInteger, StatusType) {
    StatusSeeding = 6,
    StatusDownloading = 4,
    StatusQueued = 3,
    StatusPaused = 0,
};

typedef NS_ENUM(NSInteger, SettingType) {
    SettingTextField,
    SettingNumberField,
    SettingPasswordField,
    SettingSwitch,
    SettingCheck
};

typedef NS_ENUM(NSInteger, SettingKey) {
    SettingKeyRpcHost,
    SettingKeyRpcPort,
    SettingKeyRpcPath,
    SettingUserName,
    SettingPassword,
    SettingKeyDownloadDir

};



@interface TCSConstants : NSObject
@end