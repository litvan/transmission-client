//
// Created by Ivan Litovchenko on 9/13/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol TCSTracker

@end


@interface TCSTracker : JSONModel

@property(nonatomic, strong) NSString *announce;
@property(nonatomic, strong) NSNumber *id;
@property(nonatomic, strong) NSString *scrape;
@property(nonatomic, strong) NSNumber *tier;

@end