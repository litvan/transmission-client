//
// Created by Ivan Litovchenko on 9/12/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol TCSFileStats

@end


@interface TCSFileStats : JSONModel

@property(nonatomic, assign) uint wanted;
@property(nonatomic, assign) uint32_t bytesCompleted;
@property(nonatomic, assign) uint priority;

@end