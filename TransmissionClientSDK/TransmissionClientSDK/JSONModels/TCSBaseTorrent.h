//
// Created by Ivan Litovchenko on 5/13/15.
// Copyright (c) 2015 Ivan Litovchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSConstants.h"
#import "JSONModel.h"


@interface TCSBaseTorrent : JSONModel

@property(nonatomic, strong) NSNumber *id;
@property(nonatomic, strong) NSNumber *downloadedEver;
@property(nonatomic, strong) NSNumber *sizeWhenDone;
@property(nonatomic, strong) NSNumber *uploadedEver;
@property(nonatomic, strong) NSString *downloadDir;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *percentDone;
@property(nonatomic, assign) StatusType status;
@property(nonatomic, strong) NSNumber *rateDownload;
@property(nonatomic, strong) NSNumber *rateUpload;

@end