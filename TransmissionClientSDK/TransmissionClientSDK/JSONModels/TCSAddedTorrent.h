//
// Created by Ivan Litovchenko on 9/9/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"


@interface TCSAddedTorrent : JSONModel

@property(nonatomic, strong) NSNumber *id;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *hashString;

@end