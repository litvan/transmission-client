//
// Created by Ivan Litovchenko on 9/12/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol TCSPeer

@end


@interface TCSPeer : JSONModel

@property(nonatomic, strong) NSString *address;
@property(nonatomic, strong) NSString *clientName;
@property(nonatomic, strong) NSNumber *clientIsChoked;
@property(nonatomic, strong) NSNumber *clientIsInterested;
@property(nonatomic, strong) NSString *flagStr;
@property(nonatomic, strong) NSNumber *isDownloadingFrom;
@property(nonatomic, strong) NSNumber *isEncrypted;
@property(nonatomic, strong) NSNumber *isIncoming;
@property(nonatomic, strong) NSNumber *isUploadingTo;
@property(nonatomic, strong) NSNumber *isUTP;
@property(nonatomic, strong) NSNumber *peerIsChoked;
@property(nonatomic, strong) NSNumber *peerIsInterested;
@property(nonatomic, strong) NSNumber *port;
@property(nonatomic, strong) NSNumber *progress;
@property(nonatomic, strong) NSNumber *rateToClient;
@property(nonatomic, strong) NSNumber *rateToPeer;

@end