//
// Created by Ivan Litovchenko on 9/8/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSBaseTorrent.h"

@class TCSFile;
@protocol TCSFile;
@protocol TCSFileStats;
@protocol TCSPeer;
@protocol TCSTracker;


@interface TCSFullTorrent : TCSBaseTorrent

@property(nonatomic, strong) NSDate *activityDate;
@property(nonatomic, strong) NSDate *addedDate;
@property(nonatomic, strong) NSNumber *bandwidthPriority;
@property(nonatomic, strong) NSString *comment;
@property(nonatomic, strong) NSNumber *corruptEver;
@property(nonatomic, strong) NSString *creator;
@property(nonatomic, strong) NSDate *dateCreated;
@property(nonatomic, strong) NSNumber *desiredAvailable;
@property(nonatomic, strong) NSDate *doneDate;
@property(nonatomic, strong) NSNumber *downloadLimit;
@property(nonatomic, strong) NSNumber *downloadLimited;
@property(nonatomic, strong) NSNumber *error;
@property(nonatomic, strong) NSString *errorString;
@property(nonatomic, strong) NSNumber *eta;
@property(nonatomic, strong) NSArray  <TCSFile> *files;
@property(nonatomic, strong) NSArray <TCSFileStats> *fileStats;
@property(nonatomic, strong) NSString *hashString;
@property(nonatomic, strong) NSNumber *haveUnchecked;
@property(nonatomic, strong) NSNumber *haveValid;
@property(nonatomic, strong) NSNumber *honorsSessionLimits;
@property(nonatomic, strong) NSNumber *isPrivate;
@property(nonatomic, strong) NSNumber *leftUntilDone;
@property(nonatomic, strong) NSNumber *manualAnnounceTime;
@property(nonatomic, strong) NSNumber *maxConnectedPeers;
@property(nonatomic, strong) NSArray <TCSPeer> *peers;
@property(nonatomic, strong) NSNumber *peersConnected;
@property(nonatomic, strong) NSDictionary *peersFrom;
@property(nonatomic, strong) NSNumber *peersGettingFromUs;
@property(nonatomic, strong) NSNumber *peersSendingToUs;
@property(nonatomic, strong) NSString *pieces;
@property(nonatomic, strong) NSNumber *pieceCount;
@property(nonatomic, strong) NSNumber *pieceSize;
@property(nonatomic, strong) NSArray *priorities;
@property(nonatomic, strong) NSNumber *recheckProgress;
@property(nonatomic, strong) NSNumber *seedRatioLimit;
@property(nonatomic, strong) NSNumber *seedRatioMode;
@property(nonatomic, strong) NSDate *startDate;
@property(nonatomic, strong) NSArray<TCSTracker> *trackers;
@property(nonatomic, strong) NSNumber *totalSize;
@property(nonatomic, strong) NSString *torrentFile;
@property(nonatomic, strong) NSNumber *uploadLimit;
@property(nonatomic, strong) NSNumber *uploadLimited;
@property(nonatomic, strong) NSNumber *uploadRatio;
@property(nonatomic, strong) NSArray *wanted;
@property(nonatomic, strong) NSArray *webseeds;
@property(nonatomic, strong) NSNumber *webseedsSendingToUs;
@property(nonatomic, strong) NSString <Optional> *announceResponse;
@property(nonatomic, strong) NSString <Optional> *announceURL;
@property(nonatomic, strong) NSNumber <Optional> *swarmSpeed;
@property(nonatomic, strong) NSNumber <Optional> *timesCompleted;
@property(nonatomic, strong) NSString <Optional> *scrapeResponse;
@property(nonatomic, strong) NSString <Optional> *scrapeURL;
@property(nonatomic, strong) NSNumber <Optional> *seeders;
@property(nonatomic, strong) NSNumber <Optional> *peersKnown;
@property(nonatomic, strong) NSNumber <Optional> *nextAnnounceTime;
@property(nonatomic, strong) NSNumber <Optional> *nextScrapeTime;
@property(nonatomic, strong) NSNumber <Optional> *peerLimit;
@property(nonatomic, strong) NSNumber <Optional> *lastAnnounceTime;
@property(nonatomic, strong) NSNumber <Optional> *lastScrapeTime;
@property(nonatomic, strong) NSNumber <Optional> *leechers;
@property(nonatomic, strong) NSNumber <Optional> *downloaders;

@end