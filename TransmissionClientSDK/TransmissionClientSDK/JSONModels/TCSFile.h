//
// Created by Ivan Litovchenko on 9/8/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol TCSFile
@end


@interface TCSFile : JSONModel

@property(nonatomic, strong) NSNumber *bytesCompleted;
@property(nonatomic, strong) NSNumber *length ;
@property(nonatomic, strong) NSString *name;

@end