//
// Created by Ivan Litovchenko on 9/18/15.
// Copyright (c) 2015 litvan.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSConstants.h"
#import "JSONModel.h"


@interface TCSSetting : JSONModel

@property(nonatomic, strong) NSString *name;
@property(nonatomic, assign) SettingType type;
@property(nonatomic, strong) NSString *value;

@end